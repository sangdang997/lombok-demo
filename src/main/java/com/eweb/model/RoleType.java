package com.eweb.model;

public enum RoleType {
	ROLE_ADMIN,
	ROLE_USER
}
