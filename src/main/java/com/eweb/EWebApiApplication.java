package com.eweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EWebApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EWebApiApplication.class, args);
	}

}
